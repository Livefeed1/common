const jwt = require('jsonwebtoken');
const HttpStatus = require('http-status-codes');

function getAuthToken(req) {
  // Get the authorization header from the request object
  const authorizationHeader = req.headers.authorization;
  if (!authorizationHeader) {
    return null;
  }
  // The header should be of the format "bearer <auth-token>". Strip
  // the first part.
  const parts = authorizationHeader.split(' ');
  if (parts.length !== 2 || parts[0].toLowerCase() !== 'bearer') {
    return null;
  }
  // The second part should contain the auth token.
  return parts[1];
}

/**
 * Authentication middleware factory
 */
module.exports = function makeRequireAuthentication(authTokenSecret, decodeOptions) {
  return (req, res, next) => {
    // Get the token from the request headers
    const authToken = getAuthToken(req);
    if (!authToken) {
      res.send(HttpStatus.UNAUTHORIZED, { type: 'INVALID_AUTH_TOKEN', message: 'Invalid auth token' });
    } else {
      // Decode and validate the token.
      let decodedToken;
      try {
        decodedToken = jwt.verify(authToken, authTokenSecret, decodeOptions);
        // Done. Attach session data to the res object and continue
        res.session = Object.assign({}, {authToken}, decodedToken);
        return next();
      } catch (err) {
        res.send(HttpStatus.UNAUTHORIZED, { type: 'INVALID_AUTH_TOKEN', message: 'Invalid auth token' });
      }
    }
  };
};
