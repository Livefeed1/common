const jwt = require('jsonwebtoken');
const ServiceError = require('../error/ServiceError');
const uuidv4 = require('uuid/v4');

/**
 * Create JWT
 * @param { Object } tokenData (payload)
 * @param { Object } signOptions
 * @param { string } secret
 * @returns { Object } The constructed token.
 * Generated error by jsonwebtoken module:
 err = {
     name: 'TokenExpiredError',
     message: 'jwt expired',
     expiredAt: 1408621000
   }
 */
exports.createJwt = (tokenData, signOptions, secret) => new Promise((resolve, reject) => {
  jwt.sign(tokenData, secret, signOptions, (err, jwToken) => {
    if (err) {
      return reject(new ServiceError(err.name, err.message, { expiredAt: err.expiredAt }));
    }
    return resolve(jwToken);
  });
});

/**
 * Verify token passed in
 * @param { string } pJwt
 * @param { string } secret
 * @returns { Object } decoded jwt object
 */
exports.verifyJwt = (pJwt, secret) => new Promise((resolve, reject) => {
  jwt.verify(pJwt, secret, (err, decoded) => {
    if (err) {
      reject(err);
    } else {
      resolve(decoded);
    }
  });
});

/**
 * Create refresh token
 * @returns { string } refresh token
 */
exports.createRefreshToken = () => uuidv4();
