
/**
 * DefaultDto
 * Used for simply getting / setting fields
 */
class BaseDto {
  constructor(fields, obj) {
    this.fields = fields;
    for (let i = 0; i < fields.length; i += 1) {
      this[fields[i]] = obj[fields[i]];
    }
  }

  toJson() {
    const returnObj = {};
    for (let i = 0; i < this.fields.length; i += 1) {
      if (typeof (this[this.fields[i]]) !== 'undefined') {
        returnObj[this.fields[i]] = this[this.fields[i]];
      }
    }
    return returnObj;
  }

}

module.exports = BaseDto;
