
const DtoCollection = require('./DtoCollection');

const mapDto = (row, DtoClass) => {
  if (row) {
    return new DtoClass(row);
  }
  return undefined;
};

const mapDtos = (rows, DtoClass) => {
  if (rows && rows.length > 0) {
    return new DtoCollection(rows, DtoClass);
  }
  return new DtoCollection([]);
};

module.exports = {
  mapDto,
  mapDtos,
};
