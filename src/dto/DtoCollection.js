
/**
 * DefaultDtoCollection
 * Used for getting and setting a collection of Dtos
 */
class DtoCollection {
  constructor(rows, DtoClass) {
    this.dtos = [];
    for (let i = 0; i < rows.length; i += 1) {
      this.dtos.push(new DtoClass(rows[i], DtoClass.fields));
    }
  }

  toJson() {
    const returnDtos = [];
    for (let i = 0; i < this.dtos.length; i += 1) {
      returnDtos[i] = this.dtos[i].toJson();
    }
    return returnDtos;
  }

}

module.exports = DtoCollection;
