/**
 * Custom error object for errors.
 */
class ServiceError {
  constructor(type, message, data) {
    this.name = 'ServiceError';
    this.type = type;
    this.message = message;
    this.data = data;
  }
}

module.exports = ServiceError;
