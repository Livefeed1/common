
const AppConfig = require('../config/AppConfig');
const HashIds = require('hashids');

const salt = AppConfig.hashids.salt;
const minLength = AppConfig.hashids.minLength;
const hashIds = new HashIds(salt, minLength);

exports.encodeHashId = id => hashIds.encode(id);

exports.decodeHashId = hashedId => hashIds.decode(hashedId)[0];
