
const { setPgTypeParsers } = require('./parsers');
const camelCaseKeys = require('camelcase-keys');
const DefaultDtoMapper = require('../dto/DefaultDtoMapper');
const { ROW_SINGLE, ROWS_MULTIPLE } = require('./QueryTypes');

const mapDto = (res, queryMode, DtoClass, DtoMapper) => {
  if (DtoClass) {
    const mapper = DtoMapper || DefaultDtoMapper;
    if (queryMode === ROW_SINGLE && res.row) {
      res.dto = mapper.mapDto(res.row, DtoClass);
    } else {
      res.dtos = mapper.mapDtos(res.rows, DtoClass);
    }
  }
  return res;
};


class PgHandler {
  constructor(pool, options) {
    if (options) {
      this.debug = options.debug;
    }
    this.pool = pool;
    setPgTypeParsers();
  }

  async executeQuery(client, text, params, queryMode) {
    const start = Date.now();
    try {
      const res = await client.query({ text, values: params });
      const duration = Date.now() - start;
      if (queryMode === ROW_SINGLE) {
        if (res.rows[0]) res.row = camelCaseKeys(res.rows[0]);
      } else {
        for (let i = 0; i < res.rows.length; i += 1) {
          res.rows[i] = camelCaseKeys(res.rows[i]);
        }
      }

      if (this.debug) {
        console.log('executed query', { text, duration, rows: res.rowCount });
      }
      return res;
    } catch (err) {
      console.log(err);
    }
    return null;
  }

  async query(text, params) {
    await this.executeQuery(this.pool, text, params);
  }

  async queryRow(text, params, DtoClass, DtoMapper) {
    const res = await this.executeQuery(this.pool, text, params, ROW_SINGLE);
    return mapDto(res, ROW_SINGLE, DtoClass, DtoMapper);
  }

  async queryRows(text, params, DtoClass, DtoMapper) {
    const res = await this.executeQuery(this.pool, text, params, ROWS_MULTIPLE);
    return mapDto(res, ROWS_MULTIPLE, DtoClass, DtoMapper);
  }

  async queryTransactionRow(client, text, params, DtoClass, DtoMapper) {
    const res = await this.executeQuery(client, text, params, ROW_SINGLE);
    return mapDto(res, ROW_SINGLE, DtoClass, DtoMapper);
  }

  async queryTransactionRows(client, text, params, DtoClass, DtoMapper) {
    const res = await this.executeQuery(client, text, params, ROWS_MULTIPLE);
    return mapDto(res, ROWS_MULTIPLE, DtoClass, DtoMapper);
  }
}

module.exports = PgHandler;
