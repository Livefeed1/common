
const pgTypes = require('pg').types;
const moment = require('moment');

const DATE_OID = 1082;
const TIMESTAMP_OID = 1114;
const INT8_OID = 20;

const parseDateFn = val => moment(val).format('YYYY-MM-DD');
const parseTimestampFn = val => moment(val).format('YYYY-MM-DD HH:mm:ss');
const parseIntFn = val => parseInt(val, 10);

exports.setPgTypeParsers = () => {
  pgTypes.setTypeParser(DATE_OID, parseDateFn);
  pgTypes.setTypeParser(TIMESTAMP_OID, parseTimestampFn);
  pgTypes.setTypeParser(INT8_OID, parseIntFn);
};
