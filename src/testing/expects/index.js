
const moment = require('moment');
const expect = require('chai').expect;
const AppConfig = require('../../config/AppConfig');

/* eslint-disable no-param-reassign */
exports.objectsToHaveSameValues = (obj1, obj2) => {
  Object.keys(obj1).forEach((key) => {
    if (obj1[key] && obj2[key]) {
      if (moment(obj1[key], moment.ISO_8601, true).isValid()) {
        obj1[key] = moment(obj1[key]).format('YYYY-MM-DD HH:mm:ss');
        obj2[key] = moment(obj2[key]).format('YYYY-MM-DD HH:mm:ss');
      }
      expect(obj1[key]).to.equal(obj2[key]);
    }
  });
};

exports.toBeHashedId = (hashedId) => {
  expect(hashedId).to.be.a('string');
  expect(hashedId).have.length.above(AppConfig.hashids.minLength - 1); // Test
};
